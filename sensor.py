"""
Cowboy Bike Support.

This provides a component for the Cowboy Bike.
"""
import logging
import datetime as datetime

from homeassistant.helpers.entity import Entity
import custom_components.cowboy as cowboy
import pprint

_LOGGER = logging.getLogger(__name__)


def setup_platform(hass, config, add_entities, discovery_info=None):
    """Setup sensors."""
    _cowboy = hass.data[cowboy.COWBOY_HANDLE]

    sensor_items = []
    sensor_items.extend([CowboySensor(hass,
                                      'total_distance',
                                      'map-marker-distance',
                                      'KM'),
                         CowboySensor(hass,
                                      'total_duration',
                                      'clock-outline',
                                      's'),
                         CowboySensor(hass,
                                      'total_co2_saved',
                                      'cloud-check',
                                      'g'),
                         CowboySensor(hass,
                                      'bike_total_distance',
                                      'map-marker-distance',
                                      'KM'),
                         CowboySensor(hass,
                                      'bike_total_duration',
                                      'clock-outline',
                                      's'),
                         CowboySensor(hass,
                                      'bike_total_co2_saved',
                                      'cloud-check',
                                      'g'),
                         CowboySensor(hass,
                                      'bike_state_of_charge',
                                      'battery',
                                      '%'),
                         CowboySensor(hass,
                                      'bike_state_of_charge_internal',
                                      'battery',
                                      '%')])

    add_entities(sensor_items)


class CowboySensor(Entity):
    """Representation of a sensor."""

    def __init__(self, hass, name, icon, unit_of_measurement):
        """Initialize the sensor."""
        self._state = None
        self._icon = "mdi:" + icon
        self._unit_of_measurement = unit_of_measurement
        self.cowboy = hass.data[cowboy.COWBOY_HANDLE]
        self._name = name

    @property
    def should_poll(self):
        """Polling required."""
        return True

    @property
    def name(self):
        """Return the name of the sensor."""
        return self._name

    @property
    def icon(self):
        """Return the mdi icon of the sensor."""
        return self._icon

    @property
    def state(self):
        """Return the state of the sensor."""
        return self.cowboy.get_data(self.name.lower())

    @property
    def device_state_attributes(self):
        """Return the state attributes."""
        attrs = {}

        attrs["nickname"] = self.cowboy.get_data("bike_nickname")
        attrs['device_id'] = self.cowboy.get_data("bike_id")
        attrs['firmware'] = self.cowboy.get_data("bike_firmware")

        return attrs

    @property
    def unit_of_measurement(self):
        """Return the unit this state is expressed in."""
        return self._unit_of_measurement

    def update(self):
        _LOGGER.debug("update of sensor {}".format(self._name))
        """Get the latest data from the sensor."""
        self.cowboy.update()
