# cowboy-ha

Installation
===

On your Home Assistant instance, go to \<config directory\>/custom_components. Clone this repository and rename the from `cowboy-ha` to `cowboy`

Configuration
===

To add Cowboy to Home Assistant, add the following to your configuration.yaml file:

```yaml
# Example configuration.yaml entry
cowboy:
    username: YOUR_USERNAME
    password: YOUR_PASSWORD
```
- **username** (*Required*): Username for Cowboy.
- **password** (*Required*): Password for Cowboy.
