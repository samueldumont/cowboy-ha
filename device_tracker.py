"""Support for tracking Cowboy Bikes."""
import logging

from homeassistant.helpers.event import track_utc_time_change
from homeassistant.util import slugify

from homeassistant.helpers.entity import Entity
import custom_components.cowboy as cowboy
import pprint

_LOGGER = logging.getLogger(__name__)


def setup_scanner(hass, config, see, discovery_info=None):
    """Set up the Tesla tracker."""
    CowboyDeviceTracker(
        hass, config, see,
        hass.data[cowboy.COWBOY_HANDLE])
    return True


class CowboyDeviceTracker:
    """A class representing a Cowboy device."""

    def __init__(self, hass, config, see, cowboy):
        """Initialize the Tesla device scanner."""
        self.hass = hass
        self.see = see
        self.cowboy = cowboy
        self._update_info()

        track_utc_time_change(
            self.hass, self._update_info, second=range(0, 60, 30))

    def _update_info(self, now=None):
        """Update the device info."""
        self.cowboy.update()
        name = self.cowboy.get_data("bike_nickname")
        _LOGGER.debug("Updating device position: %s", name)
        dev_id = slugify(name)
        location = self.cowboy.get_data("bike_position")
        if location:
            lat = location['latitude']
            lon = location['longitude']
            accuracy = location['accuracy']
            source = location['source']
            battery = self.cowboy.get_data("bike_state_of_charge")
            attrs = {
                'trackr_id': dev_id,
                'id': dev_id,
                'name': name,
                'source_type': source
            }
            self.see(
                dev_id=dev_id, host_name=name,
                gps=(lat, lon), attributes=attrs,
                gps_accuracy=accuracy, battery=battery
            )
