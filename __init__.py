"""
Cowboy Bike Support.

This provides a component for the Cowboy Bike.
"""
import logging
from datetime import datetime, timedelta
import voluptuous as vol

# Import the device class from the component that you want to support
from homeassistant.const import (CONF_USERNAME, CONF_PASSWORD)
from homeassistant.helpers.discovery import load_platform
import homeassistant.helpers.config_validation as cv
from homeassistant.util import Throttle
import pprint

# Home Assistant depends on 3rd party packages for API specific code.
REQUIREMENTS = ['cowboybike==0.0.5']

_LOGGER = logging.getLogger(__name__)

MIN_TIME_BETWEEN_UPDATES = timedelta(seconds=180)

DOMAIN = 'cowboy'
COWBOY_HANDLE = "cowboy_handle"

# Validation of the user's configuration
CONFIG_SCHEMA = vol.Schema({
    DOMAIN: vol.Schema({
        vol.Required(CONF_USERNAME): cv.string,
        vol.Required(CONF_PASSWORD): cv.string
    }),
}, extra=vol.ALLOW_EXTRA)


def setup(hass, config):
    """Setup toon."""
    _LOGGER.debug("Initializing Cowboy")
    try:
        hass.data[COWBOY_HANDLE] = CowboyDatastore(config['cowboy'][CONF_USERNAME],
                                                   config['cowboy'][CONF_PASSWORD])
    except Exception as e:
        _LOGGER.error(e)

    # except InvalidCredentials:
    #    _LOGGER.warning("The credentials in your config are invalid")
    #    return False

    # Load all platforms
    for platform in ('sensor', 'device_tracker'):
        load_platform(hass, platform, DOMAIN, {}, config)

    # Initialization successfull
    return True


class CowboyDatastore:
    """An object to store the cowboy data."""

    def __init__(self, username, password):
        """Initialize toon."""
        from cowboybike import Cowboy

        # Creating the class
        self.cowboy = Cowboy.with_auth(username, password)
        self.data = {}

        self.last_update = datetime.min
        self.update()

    @Throttle(MIN_TIME_BETWEEN_UPDATES)
    def update(self):
        """Update toon data."""
        self.last_update = datetime.now()

        self.cowboy.refreshData()
        self.data["total_distance"] = self.cowboy.getTotalDistance()
        self.data["total_duration"] = self.cowboy.getTotalDuration()
        self.data["total_co2_saved"] = self.cowboy.getTotalCO2Saved()
        self.data["bike_id"] = self.cowboy.getBike().getId()
        self.data["bike_nickname"] = self.cowboy.getBike().getNickname()
        self.data["bike_total_distance"] = self.cowboy.getBike(
        ).getTotalDistance()
        self.data["bike_total_duration"] = self.cowboy.getBike(
        ).getTotalDuration()
        self.data["bike_total_co2_saved"] = self.cowboy.getBike(
        ).getTotalCO2Saved()
        self.data["bike_is_stolen"] = self.cowboy.getBike().isStolen()
        self.data["bike_state_of_charge"] = self.cowboy.getBike(
        ).getStateOfCharge()
        self.data["bike_state_of_charge_internal"] = self.cowboy.getBike(
        ).getStateOfChargeInternal()
        self.data["bike_firmware"] = self.cowboy.getBike().getFirmwareVersion()
        self.data["bike_position"] = self.cowboy.getBike().getPosition()

        _LOGGER.debug(pprint.pprint(self.data))

    def get_data(self, data_id):
        """Get the cached data."""
        data = {'error': 'no data'}
        if data_id in self.data:
            data = self.data[data_id]
        return data
